# jqResponsiveClasses

A jQuery plugin to add media query based classes on DOM elements for easier responsive styling

## Basic usage
```JavaScript
$('selector').jqResponsiveClasses();
```

## Options
By default the plugin uses Bootstrap's breakpoints
```JavaScript
// { class_name: media_query }
{
    xs: "(min-width: 0px) and (max-width: 575px)",
    sm: "(min-width: 576px) and (max-width: 767px)",
    md: "(min-width: 768px) and (max-width: 991px)",
    lg: "(min-width: 992px) and (max-width: 1199px)",
    xl: "(min-width: 1200px) and (max-width: 1399px)",
    xx: "(min-width: 1400px)"
}
```

You can override the defaults by passing an options object with two properties `breakpoints` and `mediaQueries`. This is what it would look like:
```JavaScript
var options = {
    breakpoints: {
    //  class: max_width
        small: 768,
        large: 1200,
        huge: Infinity
    },
    mediaQueries: {
    //  class: media query
        mobile: "(max-width: 575px)",
        portrait: "(orientation: portrait)"
    }
};

$('selector').jqResponsiveClasses(options);

/* Resulting  { class: media query } set
{
    small: "(min-width: 0) and (max-width: 767px)",
    large: "(min-width: 768px) and (max-width: 1199px)",
    huge: "(min-width: 1200px)",
    mobile: "(max-width: 575px)",
    portrait: "(orientation: portrait)"
}
*/
```

### Caveats:

*1. Providing a set of `breakpoints` to the options will override the defaults. To keep the default breakpoints and classes only set the `mediaQueries` property during plugin initialisation.*

*2. Classes defined in `mediaQueries` will override those in `breakpoints` if they happen to be the same.*