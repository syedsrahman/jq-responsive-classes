/**
 * @author Syed S. Rahman <syedsrahman@fastmail.com>
 * @license MIT
 */

(function ($) {
  var defaults = {
    breakpoints: {
      xs: 576,
      sm: 768,
      md: 992,
      lg: 1200,
      xl: 1400,
      xx: Infinity
    }
  };

  $.fn.jqResponsiveClasses = function responsiveClasses(o) {
    var options = $.extend({}, $.fn.jqResponsiveClasses.defaults, o);
    var mediaQueries = $.extend(
      {},
      breakpointsToMediaQueries(options.breakpoints),
      options.mediaQueries
    );

    for (var className in mediaQueries) {
      var mql = window.matchMedia(mediaQueries[className]);
      var cb = handleMQChange.bind(this, className);
      mql.addEventListener('change', cb);
      cb(mql);
    }
    return this;
  };

  $.fn.jqResponsiveClasses.defaults = defaults;

  function handleMQChange(className, e) {
    this.toggleClass(className, e.matches);
  }

  function breakpointsToMediaQueries(breakpoints) {
    var classNames = Object.keys(breakpoints);
    return classNames
      .map(function toTuple(className) {
        // [className, maxWidth]
        return [className, breakpoints[className]];
      })
      .sort(function byMaxWidthAsc(a, b) {
        if (a[1] < b[1]) { return -1; }
        if (a[1] > b[1]) { return 1; }
        return 0;
      })
      .map(function toMinMaxTuple(val, i, list) {
        // [className, minWidth, maxWidth]
        return [val[0], i === 0 ? i : list[i - 1][1], val[1] - 1];
      })
      .reduce(function toMQObject(mq, val, index, list) {
        var className = val[0];
        var minWidth = val[1];
        var maxWidth = val[2];
        mq[className] =
          "(min-width: " +
          minWidth +
          "px)" +
          (isFinite(maxWidth) ? " and (max-width: " + maxWidth + "px)" : "");
        return mq;
      }, {});
  }
})(jQuery);